package org.harouna.model.Exercice4;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.harouna.model.Exercice3.*;

@Embeddable
public class Adresse {

	@Column(name="rue", length = 40)
	private String rue;

	@ManyToOne
	private Commune commune;

	@OneToOne
	private Maire maire;

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public Commune getCommune() {
		return commune;
	}

	public void setCommune(Commune commune) {
		this.commune = commune;
	}
}
