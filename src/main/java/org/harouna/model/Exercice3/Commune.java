package org.harouna.model.Exercice3;

import java.util.Comparator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.harouna.model.Exercice4.Adresse;

@Entity
@Table(name="t_Commune")
public class Commune implements Comparable{

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	private int id;

	@Column(name="nom", length = 40)
	private String nom;

	@Column(name="codePostal", length = 40)
	private String codePostal;

	@OneToOne
	private Maire maire;

	@ManyToOne(
			cascade=CascadeType.PERSIST, 
			fetch=FetchType.EAGER)
	private Departement departement;

	@Embedded
	private Adresse adresse;

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public Maire getMaire() {
		return maire;
	}

	public void setMaire(Maire maire) {
		this.maire = maire;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public Departement getDepartement() {
		return departement;
	}

	public void setDepartement(Departement departement) {
		this.departement = departement;
	}

	public int compareTo(Object o){
		Commune c = (Commune)o;
		return (nom.compareTo(c.getNom()));
	}

	@Override
	public String toString() {
		return "Commune [id=" + id + ", nom=" + nom + ", codePostal=" + codePostal + "]";
	}
}
