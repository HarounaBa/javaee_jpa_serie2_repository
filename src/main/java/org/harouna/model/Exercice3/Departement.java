package org.harouna.model.Exercice3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity
@Table(name= "t_Departement")
public class Departement {

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private int id;

	@Column(name="nom", length = 40)
	private String nom;

	@OneToMany(cascade=CascadeType.PERSIST, fetch=FetchType.EAGER, mappedBy="departement")
	private List<Commune> commune = new ArrayList();

	@OneToOne(cascade=CascadeType.PERSIST, fetch=FetchType.EAGER)
	@JoinFetch(JoinFetchType.OUTER)
	private Maire maire;

	public Maire getMaire(Commune commune){
		Maire m = commune.getMaire();
		return m;
	}

	public List<Maire> getMaires(){
		List<Maire> maires = new ArrayList();
		for (Commune c : commune){
			maires.add(getMaire(c));
		}
		return maires;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Commune> getCommunes(){
		Collections.sort(commune);
		return commune;
	}

	public List<Commune> getCommune() {
		return commune;
	}

	public void setCommune(List<Commune> commune) {
		this.commune = commune;
	}

	@Override
	public String toString() {
		return "Departement [id=" + id + ", nom=" + nom + ", commune=" + commune + ", getMaires()=" + getMaires() + "]";
	}
}
