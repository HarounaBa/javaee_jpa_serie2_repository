package org.harouna.model.Exercice3;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.harouna.model.Exercice4.Adresse;

@Entity
@Table(name="t_Maire")
public class Maire {

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private int id;

	@Column(name ="nom", length = 40)
	private String nom;

	@OneToOne
	private Commune commune;

	@OneToOne
	private Departement departement;

	@Embedded
	private Adresse adresse;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}


	public Commune getCommune() {
		return commune;
	}

	public void setCommune(Commune commune) {
		this.commune = commune;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	@Override
	public String toString() {
		return "Maire [id=" + id + ", nom=" + nom + ", commune=" + commune + "]";
	}
}
