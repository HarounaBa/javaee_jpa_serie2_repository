package org.harouna.main;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.harouna.model.Exercice3.*;

public class MainExercice3 {

	public static void main(String[] args) {

		Commune commune1 = new Commune();
		commune1.setNom("Pikine");
		commune1.setCodePostal("4443");

		Commune commune2 = new Commune();
		commune2.setNom("Maristes");
		commune2.setCodePostal("432");

		Commune commune3 = new Commune();
		commune3.setNom("Rufisque");
		commune3.setCodePostal("452");

		Maire maire = new Maire();
		maire.setNom("Dupont");
		maire.setCommune(commune1);

		Departement departement = new Departement();
		commune1.setDepartement(departement);
		commune2.setDepartement(departement);
		commune3.setDepartement(departement);

		commune1.setMaire(maire);

		List<Commune> listeCommune = new ArrayList();
		listeCommune.add(commune1);
		listeCommune.add(commune2);
		listeCommune.add(commune3);

		departement.setNom("Dakar");
		departement.setCommune(listeCommune);
		List<Commune> listOrdonne = departement.getCommunes();
		departement.setCommune(listOrdonne);

				
		/********************Create**************************/
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-serie2");	
		EntityManager em = emf.createEntityManager();
	
		em.getTransaction().begin();

		em.persist(commune1);
		em.persist(commune2);
		em.persist(commune3);
		em.persist(maire);
		em.persist(departement);

		em.getTransaction().commit();

		/************************Retrieve*****************************/
		Maire m = em.find(Maire.class, 5);
		System.out.println("Maire ayant id 5 : " + m);

		/*************************Update******************************/
		em.getTransaction().begin();

		m = em.find(Maire.class, 5);
		m.setNom("El Hadj");
		
		em.getTransaction().commit();
	}
}
