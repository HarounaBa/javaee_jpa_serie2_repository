package org.harouna.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.harouna.model.Exercice3.*;
import org.harouna.model.Exercice4.Adresse;

public class MainExercice4 {

	public static void main(String[] args) {
		
		Adresse adresseMairie = new Adresse();
		adresseMairie.setRue("avenue de la république");
		
		Commune commune1 = new Commune();
		commune1.setNom("Pikine");
		commune1.setCodePostal("4443");
		commune1.setAdresse(adresseMairie);
		
		adresseMairie.setCommune(commune1);	
		Adresse adresseMaire = new Adresse();
		adresseMaire.setRue("Rue des Jardins");
		
		Maire maire = new Maire();
		maire.setNom("Dupont");
		maire.setCommune(commune1);
		maire.setAdresse(adresseMaire);
		
		commune1.setMaire(maire);
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-serie2");
		EntityManager em = emf.createEntityManager();
		
		/*****Create*****/
		em.getTransaction().begin();

		em.persist(commune1);
		em.persist(maire);
		
		em.getTransaction().commit();
	}
}
